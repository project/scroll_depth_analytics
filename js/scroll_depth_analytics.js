/**
 * @file
 * Provides Scroll Depth Analytics.
 *
 * Analytics tracking on page scrolling.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.scroll_depth_analytics = {
    attach: function (context, settings) {
      if ($.scrollDepth) {
        var elements_to_track = false;
        if (typeof Drupal.settings.scroll_depth_analytics.scroll_depth_analytics_element_scroll_elements != 'undefined') {
          elements_to_track = jQuery.parseJSON(Drupal.settings.scroll_depth_analytics.scroll_depth_analytics_element_scroll_elements);
        }
        $.scrollDepth({
          minHeight: 2000,
          elements: elements_to_track,
          percentage: Drupal.settings.scroll_depth_analytics.scroll_depth_analytics_page_scroll_visibility,
          userTiming: false,
          pixelDepth: false,
          nonInteraction: false,
          eventHandler: function (data) {
            if (typeof (_gaq) == 'object') {
              _gaq.push(['_trackEvent', data.eventCategory, data.eventAction, data.eventLabel, data.eventValue]);
            }
            else if (typeof (ga) == 'function') {
              ga('send', 'event', data.eventCategory, data.eventAction, data.eventLabel, data.eventValue);
            }
          }
        });
      }
    }
  };
})(jQuery);
